# home-vpn
Project for starting your home vpn tunnel uses https://github.com/vpnhouse/tunnel. Your should have server with Ubuntu or Debian OS.

Steps:
1) Connect to server using ssh connection.
2) Execute scripts. It installs docker, docker-compose and run vpnhouse-tunnel container (also it runs cadvisor and node-exporter for monitoring):
```
git clone https://gitlab.com/makkov/home-vpn.git
cd home-vpn
chmod +x home-vpn-start.sh
./home-vpn-start.sh
```